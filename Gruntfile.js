module.exports = function(grunt) {

    // 1. Toutes les configurations vont ici: 
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        autoprefixer: {
            build: {
                options: {
                    browsers: ['last 4 version']
                },
                files: [{
                    expand: true,
                    cwd: 'css/sass',
                    src: ['*.css', '!*.min.css'],
                    dest: 'css/sass/prefix',
                    ext: '.css'
                }, {
                    expand: true,
                    cwd: 'css/plugins',
                    src: ['*.css', '!*.min.css'],
                    dest: 'css/plugins/prefix',
                    ext: '.css',
                    extDot: 'last'   // Extensions in filenames begin after the last dot
                }]
            }
        },

        sass: {
            build: {
                options: {
                    style: 'expanded'
                },
                files: [{
                    expand: true,       // Enable dynamic expansion.
                    cwd: 'css/sass/',   // Src matches are relative to this path.
                    src: ['*.scss'],    // Actual pattern(s) to match.
                    dest: 'css/sass/',
                    ext: '.css',        // Dest filepaths will have this extension.
                }],
            }
        },

        cssmin: {
            dist: {
                files: [{
                    expand: true,
                    flatten: true,
                    cwd: 'css/plugins/prefix',
                    src: ['*.css', '!*.min.css'],
                    dest: 'css/plugins',
                    ext: '.min.css',
                    extDot: 'last'   // Extensions in filenames begin after the first dot
                }]
            },
            devel: {
                files: [{
                    expand: true,
                    flatten: true,
                    cwd: 'css/sass/prefix',
                    src: ['*.css', '!*.min.css'],
                    dest: '.',
                    ext: '.min.css'
                },
                {
                    expand: true,
                    flatten: true,
                    cwd: 'css/fonts',
                    src: ['*.css', '!*.min.css'],
                    dest: 'css',
                    ext: '.min.css'
                }]
            }
        },
/*
        concat: {
            // 2. la configuration pour la concaténation va ici.
            dist: {
                src: ['js/*.js'],
                dest: 'js/build/production.js'
            }
        },
*/
        uglify: {
            dist: {
                options: {
                    banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */',
                    preserveComments: '/^!/'
                },
                files: [{
                    expand: true,
                    cwd: 'js/',
                    src: ['**/*.js', '!**/*.min.js'],
                    dest: 'js/build',
                    ext: '.min.js',
                    extDot: 'last'   // Extensions in filenames begin after the first dot
                }]
            }
        },

        copy: {
            devel: {
                files: [{
                    expand: true,
                    cwd: 'js/build',
                    src: ['**/*.min.js'], 
                    dest: 'js/', 
                    filter: 'isFile'
                }],
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: 'css/images/build',
                    src: ['*.{png,jpg,gif}'],
                    dest: 'css/images/', 
                    filter: 'isFile'
                }],
            },
        },

        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: 'css/images/build',
                    src: ['*.{png,jpg,gif}'],
                    dest: 'css/images/'
                }]
            }
        },

        watch: {
            options: {
                livereload: true,
            },            
            scripts: {
                files: ['js/*.js'],
                tasks: ['uglify', 'copy'],
                options: {
                    spawn: false,
                },
            },
            css: {
                files: ['css/sass/*.scss', 'css/**/*.css'],
                tasks: ['sass', 'autoprefixer', 'cssmin'],
                options: {
                    spawn: false,
                }
            }
        },

    });

    // 3. Nous disons à Grunt que nous voulons utiliser ce plug-in.
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-autoprefixer');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-copy');

    // 4. Nous disons à Grunt quoi faire lorsque nous tapons "grunt" dans la console.
    grunt.registerTask('default', ['sass', 'autoprefixer', 'cssmin', 'copy', 'imagemin', 'watch']);
    grunt.registerTask('devel', ['sass', 'autoprefixer', 'cssmin', 'uglify', 'copy:devel', 'watch']);
    grunt.registerTask('prod', ['sass', 'autoprefixer', 'cssmin', 'uglify', 'copy', 'imagemin']);

};