<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package maia
 */

get_header(); ?>

<?php
    if ( have_posts() ) :
        while ( have_posts() ) : the_post();
?>
    <svg class="hidden">
        <defs>
            <filter id="blur" x="-30%" y="-30%" width="160%" height="160%">
                <feGaussianBlur stdDeviation="0" result="blur" data-min-deviation="0" data-max-deviation="10"/>
                <feMerge>
                    <feMergeNode in="blur"/>
                </feMerge>
            </filter>
        </defs>
    </svg>
    <section class="content content--scroll">
        <div class="inner">
            <header class="content__header">
                <div class="content__intro content__breakout">
                    <div class="content__intro-imgWrap">
                        <img data-scroll data-scroll-speed="1" data-scroll-delay="0.1" data-scroll-position="top" class="content__intro-img" src="<?php echo get_the_post_thumbnail_url(get_the_ID()) ?>" alt="<?the_title();?>" />
                    </div>
                </div>
                <svg class="svgtext svgtext--1 content__title anim-block-wrap" data-filter-type="blur" width="120%" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1000 200" data-scroll data-scroll-speed="2" data-scroll-delay="0.1" data-scroll-position="top">
                    <path id="text-curve1" d="M 0 100 Q 250 200 500 100 Q 750 0 1000 100" fill="none"/>
                    <text filter="url(#blur)" class="anim-block">
                        <textPath href="#text-curve1">
                        <?php 
                            the_title();
                            if (strlen (get_the_title()) < 30) {
                                echo "&nbsp;&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;";
                                the_title();
                            }
                        ?>
                        </textPath>
                    </text>
                </svg>
                <svg class="svgtext content__subtitle anim-block-wrap" data-filter-type="blur" width="120%" preserveAspectRatio="xMidYMid meet" viewBox="0 0 1000 200" data-scroll data-scroll-speed="1" data-scroll-delay="0.1" data-scroll-position="top">
                    <path id="text-curve11" d="M 0 100 Q 250 0 500 100 Q 750 200 1000 100" fill="none"/>
                    <text filter="url(#blur)" class="anim-block">
                        <textPath href="#text-curve11">
                        <?php echo get_field('sous_titre', get_the_ID()); ?>
                        </textPath>
                    </text>
                </svg>
            </header>
            <div class="content__body">
                <p class="content__body-para text-right cell--1-2">
                    <?php
                        the_content(); 
                    ?>
                </p>
            </div>
        </div>
    </section>
    <svg class="cursor" width="30" height="30" viewBox="0 0 30 30">
        <circle class="cursor__inner" cx="15" cy="15" r="7.5"/>
    </svg>

<?php
        endwhile;
    else :
        get_template_part( 'template-parts/content', 'none' );
    endif; ?>
<?php
get_footer();
