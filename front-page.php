<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package maia
 */

get_header("front"); ?>

<?php
    if ( have_posts() ) :
        while ( have_posts() ) : the_post();
?>
    <svg class="hidden">
        <defs>
            <filter id="filter-1">
                <feTurbulence type="fractalNoise" baseFrequency="0" numOctaves="1" result="warp" />
                <feOffset dx="-90" result="warpOffset" />
                <feDisplacementMap xChannelSelector="R" yChannelSelector="G" scale="30" in="SourceGraphic" in2="warpOffset" />
            </filter>
            <filter id="blur" x="-30%" y="-30%" width="160%" height="160%">
                <feGaussianBlur stdDeviation="0" result="blur" data-min-deviation="0" data-max-deviation="10"/>
                <feMerge>
                    <feMergeNode in="blur"/>
                </feMerge>
            </filter>
            <filter id="blur2" x="-30%" y="-30%" width="160%" height="160%">
                <feGaussianBlur in="SourceAlpha" stdDeviation="0" result="glow" data-min-deviation="0" data-max-deviation="30"/>
                <feColorMatrix result="bluralpha" type="matrix" values="0 -1 0 0 0 0 -1 0 0 1 0 0 -1 0 1 0 0 0 1.8 0 "/>
                <feOffset in="bluralpha" dx="0.000000" dy="0.000000" result="offsetBlur"/>
                <feMerge>
                    <feMergeNode in="offsetBlur"/>
                    <feMergeNode in="SourceGraphic"/>
                </feMerge>
            </filter>
        </defs>
        <symbol id="icon-soundon" viewBox="0 0 32 31">
            <title>Sound on</title>
            <path d="M6.405 9.239H0v13.128h6.405l12.474 8.316V.923L6.405 9.239zM5.75 20.491H1.875v-9.377h3.876v9.377zm11.252 6.687l-9.376-6.25v-10.25l9.376-6.25v22.75zm10.884-21.32c5.484 5.483 5.484 14.406 0 19.89l-1.326-1.326c4.753-4.752 4.753-12.486 0-17.239l1.326-1.326zm-2.652 2.651c4.021 4.022 4.021 10.566 0 14.587l-1.326-1.326c3.29-3.29 3.29-8.644 0-11.935l1.326-1.326zm-2.652 2.652a6.571 6.571 0 010 9.283l-1.327-1.326a4.694 4.694 0 000-6.63l1.327-1.327z"/>
        </symbol>
        <symbol id="icon-soundoff" viewBox="0 0 32 31">
            <title>Sound off</title>
            <path d="M6.409 9.11H0v13.135h6.409l12.48 8.32V.79L6.41 9.109zm-.655 11.258H1.876v-9.382h3.878v9.382zm11.26 6.691L7.63 20.804V10.55l9.382-6.255V27.06zM32 11.65l-1.327-1.326-4.027 4.027-4.028-4.027-1.327 1.326 4.028 4.028-4.028 4.028 1.327 1.327 4.027-4.028 4.028 4.028L32 19.705l-4.028-4.028L32 11.65z"/>
        </symbol>
    </svg>


    <?php 
        echo homepage_audio();
    ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

        <div class="frame">
            <h1 class="frame__pagetitle"><?php the_title(); ?></h1>
            <div class="frame__pagecontact">
                <a href="/contact">Contact/Agenda</a>
            </div>
            <button class="toggle toggle--on button" id="toggle-btn">
                <svg class="toggle__icon toggle__icon--on"><use xlink:href="#icon-soundon"></use></svg>
                <svg class="toggle__icon toggle__icon--off"><use xlink:href="#icon-soundoff"></use></svg>
            </button>
        </div>

        <?php 
            echo homepage_image(); 
            echo wp_get_menu_array("menu-1");
//            echo homepage_content("menu-1");
        ?>
        <?php
//				get_template_part( 'template-parts/content', get_post_format() );
			endwhile;
		else :
			get_template_part( 'template-parts/content', 'none' );
		endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
