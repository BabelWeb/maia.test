<?php
/**
 * Ajoute le thème parent pour éviter l'utilisation de @import en CSS
 */
function import_parent_style() {
    $version = 2.65;
    $min = ".min";

    if(!is_admin()) {
        //
        // Les CSS
        //
        wp_enqueue_style("maia",            get_bloginfo("template_directory") . "/style".$min.".css",                           "", $version, false);

        if (is_front_page() || is_page()) {
            wp_enqueue_style("splitting",       get_bloginfo("template_directory") . "/css/plugins/splitting".$min.".css",       "", $version, false);
            wp_enqueue_style("splitting-cells", get_bloginfo("template_directory") . "/css/plugins/splitting-cells".$min.".css", "", $version, false);
            wp_enqueue_style("page",            get_bloginfo("template_directory") . "/page".$min.".css",                        "", $version, false);
        }

        if (is_page("contact")) {
            wp_enqueue_style('scrollbar',       get_bloginfo("template_directory") . '/css/plugins/jquery.scrollbar.min.css',    "", $version, false);
            wp_enqueue_style("contact",         get_bloginfo("template_directory") . "/contact".$min.".css",                     "", $version, false);
        }

        if (!is_front_page() && !is_page())
            wp_enqueue_style("spectacle",       get_bloginfo("template_directory") . "/spectacle".$min.".css",                   "", $version, false);

        if (get_post_type() == "event") {
            wp_enqueue_style("events",          get_bloginfo("template_directory") . "/events".$min.".css",                      "", $version, false);
        }

        if (is_search()) {
            wp_enqueue_style("search",          get_bloginfo("template_directory") . "/search".$min.".css",                      "", $version, false);
        }
        
        //
        // Les JS
        //
        if (!is_front_page()) {
            wp_enqueue_script( 'nicescroll',       get_template_directory_uri() . '/js/lib/jquery.nicescroll.min.js',           array( 'jquery' ),       $version, true );
            wp_enqueue_script( 'scrollpage',       get_template_directory_uri() . '/js/nice-scrollbar.min.js',                  array( 'jquery' ),       $version, true );
        }

        if (is_front_page() || is_page("spectacles") || is_page("ateliers") || is_page("paroles-nomades") || is_page("cheminement")) {
            wp_enqueue_script( 'anime',            get_bloginfo("template_directory") . '/js/lib/plugins/anime.min.js',           array("jquery"),       $version, false );
            wp_enqueue_script( 'scroll',           get_bloginfo("template_directory") . '/js/lib/plugins/scrollMonitor.min.js',   array("jquery"),       $version, true );
            wp_enqueue_script( "splitting",        get_bloginfo("template_directory") . "/js/lib/plugins/splitting.min.js",       "",                    $version, false);
            wp_enqueue_script( 'maia',             get_bloginfo("template_directory") . "/js/index$min.js",                       array("imagesloaded"), $version, false);
        }

        if (is_page("spectacles") || is_page("ateliers")) {
            wp_enqueue_script( 'revealer-gal',     get_bloginfo("template_directory") . "/js/lib/plugins/revealer-gallery$min.js",array("jquery"),       $version, true );
        }
    
        if (is_page("paroles-nomades") || is_category()) {
            wp_enqueue_script( 'btn-next',         get_bloginfo("template_directory") . "/js/buttonNext$min.js",                  array("jquery"),       $version, false);
        }

        if (is_page("contact")) {
            wp_enqueue_script( 'splitting',        get_bloginfo("template_directory") . "/js/lib/plugins/splitting.min.js",       "",                    $version, false);
            wp_enqueue_script( 'jquery.scrollbar', get_bloginfo("template_directory") . '/js/lib/jquery.scrollbar.min.js',        array("jquery"),       $version, false);
            wp_enqueue_script( 'scroll',           get_bloginfo("template_directory") . "/js/lib/ScrollMagic.min.js",             array(),               $version, false);
            wp_enqueue_script( 'maia',             get_bloginfo("template_directory") . "/js/index$min.js",                       array("imagesloaded"), $version, false);
            wp_enqueue_script( 'contact',          get_bloginfo("template_directory") . "/js/contact$min.js",                     array("jquery"),       $version, false);
        }

        if (!is_front_page() && !is_page()) {
            wp_enqueue_script( "svgText",          get_bloginfo("template_directory") . "/js/svgtext$min.js",                     "",                     $version, true);
            wp_enqueue_script( 'thumb',            get_bloginfo("template_directory") . "/js/ThumbFullTransition$min.js",         "",                     $version, true);
        }

        if (get_post_type() == "post" && !is_category()) {
            wp_enqueue_script( 'sound',            get_bloginfo("template_directory") . "/js/article-sound$min.js",               "",                     $version, true);
            wp_localize_script( 'sound', 'sound_image',array( 'image_url' => get_the_category_data(wp_get_post_categories(get_the_ID())[0])->url));
        }

        wp_enqueue_script( 'menu-mobile',       get_template_directory_uri() . "/js/mobile-menu$min.js",                  array( 'jquery' ),       $version, true );

        add_filter('wpcf7_autop_or_not', '__return_false');
    }
}
add_action("wp", "import_parent_style", 0);

function add_type_attribute($tag, $handle, $src) {
    if ( 'maia' !== $handle ) {
        return $tag;
    }
    $tag = '<script type="module" src="' . esc_url( $src ) . '"></script>';
    return $tag;
}
add_filter('script_loader_tag', 'add_type_attribute' , 10, 3);


/**
 * Enqueue scripts and styles.
 *
 * @return none
*/
function import_scripts() {
    wp_enqueue_style('maia-style', get_stylesheet_directory_uri() . '/style.css');
}
add_action('wp_enqueue_scripts', 'import_scripts');

function maia_body_class($class) {
    if (is_page("cheminement"))
        $class[] = "page-cheminement";
    return $class;
}
add_action("body_class", "maia_body_class");

if ( ! function_exists( 'maia_setup' ) ) :
	function maia_setup() {
		load_theme_textdomain( 'maia', get_template_directory() . '/languages' );
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'title-tag' );
		add_theme_support( 'post-thumbnails' );

		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'maia' ),
			'menu-2' => esc_html__( 'Secondary', 'maia' ),
			'menu-3' => esc_html__( 'Blog', 'maia' ),
		) );

		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		add_theme_support( 'custom-background', apply_filters( 'maia_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		add_theme_support( 'customize-selective-refresh-widgets' );

		add_theme_support( 'custom-logo', array(
			'height'      => 50,
			'width'       => 50,
			'flex-width'  => true,
			'flex-height' => true,
        ) );

        add_theme_support('category-thumbnails');
	}
endif;
add_action( 'after_setup_theme', 'maia_setup' );

/**
 * Intégrer menu order dans l'interface admin.
 */
// register the column
function maia_columns($columns) {
    $columns['order'] = 'Order';
    return $columns;
}
add_filter('manage_edit-post_columns', 'maia_columns');

// Display the column content
function maia_show_columns($name) {
    global $post;

    switch ($name) {
        case 'order':
            $views = $post->menu_order;
            echo $views;
            break;
    }
}
add_action('manage_posts_custom_column',  'maia_show_columns');

// Register the column as sortable
function maia_column_register_sortable( $columns ) {
    $columns['order'] = 'order';

    return $columns;
}
add_filter( 'manage_edit-post_sortable_columns', 'maia_column_register_sortable' );


add_filter('posts_distinct', 'search_distinct');
function search_distinct() {
    if (is_search())
    	return "DISTINCT";
    else    
        return;
}

function homepage_audio() {
    $html     = "";
    $hp_ID    = get_option('page_on_front');
    $lot_sons = get_field( 'lot_sons', $hp_ID);

    foreach ($lot_sons as $son_id => $son_data) {
        $html .= sprintf("<audio src='%s' id='%s'></audio>", $son_data["url"], $son_id);
    }

    return $html;
}

function homepage_image() {
    $html     = '<div class="screens" aria-hidden="true">';
    $hp_ID    = get_option('page_on_front');
    $lot_imgs = get_field( 'lot_image', $hp_ID);

    foreach ($lot_imgs as $image_id) {
	    $url = wp_get_attachment_image_src( $image_id, 'full' );
        $html .= sprintf('<div class="screen__item" style="--bg-img:url(%s);">
        <div class="screen screen--full"></div>
        <div class="screen screen--clip screen--clip-1"></div>
    </div>', $url[0]);
    }

    $html .= '</div> <!-- screens -->';

    return $html;
}

function wp_get_menu_array($current_menu) {
    $html = '<div class="items"><nav class="menu menu--line">';

    $array_menu = wp_get_nav_menu_items($current_menu);

    foreach ($array_menu as $m) {
        $object_id = get_post_meta( $m->ID, '_menu_item_object_id', true );
        $url = $m->url;
        $subtitle  = get_field('sous_titre', $object_id);

        $html .= sprintf('<span class="menu__item" href="#">
        <a class="menu__link menu__link--outline menu__link--1 menu__item-link" href="%s" >%s</a>
        <div class="item__excerpt">
            <a class="item__excerpt-link" data-url="%s"><span>%s</span></a>
        </div>
        </span>', esc_attr($url), $m->title, esc_attr($url), $subtitle);
    }

    $html .= "</nav></div><!-- .items -->";
    return $html;
     
}

function homepage_content ($current_menu) {
    $html = '<section class="content">';

    $array_menu = wp_get_nav_menu_items($current_menu);

    foreach ($array_menu as $m) {
        $object_id = get_post_meta( $m->ID, '_menu_item_object_id', true );

        $subtitle  = get_field('sous_titre', $object_id);
//        $post      = get_post($object_id); // specific post
//        $content   = apply_filters('the_content', $post->post_content);
        $content   = apply_filters( 'the_content', get_the_content("", "", $object_id) );
        $title     = $m->title;

        $html .= sprintf('<article class="content__article" id="content-%s">
        <h2 class="heading">
            <span data-splitting>%s</span>
            <span data-splitting>%s</span>
        </h2>
        <div class="content__text">%s</div>
    </article>', $object_id, $title, $subtitle, $content);
    }

    $html .= sprintf('<button class="content__back unbutton" data-home="%s"><svg width="108" height="23" viewBox="0 0 108 23">
        <path stroke="#000" fill="none" d="M107.5 11.5H1.5M1.5 11.5c8.975-.536 15.087-1.364 18.336-2.484C23.086 7.896 26.64 5.39 30.5 1.5M1.5 11.5c8.975.536 15.087 1.364 18.336 2.484 3.25 1.12 6.804 3.626 10.664 7.516"/>
    </svg></button>', get_home_url());

    $html .= "</section> <!-- .content -->";
    return $html;
     
}

function hook_css() {

?>
<style>
.wp-embed-featured-image.square {
    float: none !important;
    max-width: 100% !important;
    margin-right: 0 !important;
}
</style>
<?php
}
add_action('wp_head', 'hook_css');

/**
 * Shortcode pour afficher les spectacles
 */
add_shortcode('retrieve_spectacles', 'maia_spectacles');
function maia_spectacles () {
    $html = "";
    $output = "";
    $index = 1;

    /**
     * Je récupère les CPT spectacles
     */
    $spectacles = get_posts (
        array (
            'post_status'           => 'publish',
            'post_type'             => 'spectacle',
            'posts_per_page'        => -1,
            'no_found_rows'         => true,
            'orderby'               => 'order',
            'order'                 => 'ASC'
        )
    );

    if (count($spectacles)) {
        foreach ($spectacles as $spectacle) {
            $pe_title      = apply_filters('the_title',$spectacle->post_title);
            $pe_content    = apply_filters('the_excerpt',$spectacle->post_excerpt);
            $pe_url        = get_post_permalink($spectacle->ID);

            if ( get_the_post_thumbnail($spectacle->ID) ) {
                $html .= sprintf("<spectacle id='post-%d' class='%s'>", $spectacle->ID, implode(' ', get_post_class('', $spectacle->ID)));
                $html .= sprintf("<div class='dual rev-%d'>", $index);
                $html .= sprintf("<a href='%s' rel='bookmark'>", esc_url( $pe_url ));
                $html .= sprintf("<div class='dual__inner' id='rev-%d'>", $index);
                $html .= sprintf("<div class='dual__half' style='background-image:url(%s);display:none;'></div>", get_the_post_thumbnail_url($spectacle->ID,  "medium_large" ));
                $html .= sprintf("</div><div class='dual__content'><h2 class='entry-title'>%s</h2>%s</div></a></div></spectacle>", $pe_title, $pe_content);
            } else {
                $html .= sprintf("<div class='entry-content maia-spectacle-entry'><a href='%s' target='_blank'>%s<h4>%s</h4>%s</a></div>",$pe_url, $pe_title, $pe_content);
            }
            $index++;
        }

    } else {
        $html .= sprintf("<p>Pas de spectacle en ce moment.</p>");
    }

    if ($html)
        $output = sprintf("<div class='maia-spectacles-container'>%s</div>", $html);
    return $output;
}

/**
 * Shortcode pour afficher les ateliers
 */
add_shortcode('retrieve_ateliers', 'maia_ateliers');
function maia_ateliers () {
    $html = "";
    $output = "";
    $index = 1;

    /**
     * Je récupère les CPT ateliers
     */
    $ateliers = get_posts (
        array (
            'post_status'           => 'publish',
            'post_type'             => 'atelier',
            'posts_per_page'        => -1,
            'no_found_rows'         => true,
            'orderby'               => 'order',
            'order'                 => 'ASC'
        )
    );

    if (count($ateliers)) {
        foreach ($ateliers as $atelier) {
            $pe_title      = apply_filters('the_title', $atelier->post_title);
            $pe_content    = apply_filters('the_excerpt', $atelier->post_excerpt);
            $pe_url        = get_post_permalink($atelier->ID);

            if ( get_the_post_thumbnail($atelier->ID) ) {
                $html .= sprintf("<spectacle id='post-%d' class='%s'>", $atelier->ID, implode(' ', get_post_class('', $atelier->ID)));
                $html .= sprintf("<div class='dual rev-%d'>", $index);
                $html .= sprintf("<a href='%s' rel='bookmark'>", esc_url( $pe_url ));
                $html .= sprintf("<div class='dual__inner' id='rev-%d'>", $index);
                $html .= sprintf("<div class='dual__half' style='background-image:url(%s);display:none;'></div>", get_the_post_thumbnail_url($atelier->ID,  "medium_large" ));
                $html .= sprintf("</div><div class='dual__content'><h2 class='entry-title'>%s</h2>%s</div></a></div></spectacle>", $pe_title, $pe_content);
            } else {
                $html .= sprintf("<div class='entry-content maia-spectacle-entry'><a href='%s' target='_blank'>%s<h4>%s</h4>%s</a></div>",$pe_url, $pe_title, $pe_content);
            }
            $index++;
        }

    } else {
        $html .= sprintf("<p>Pas d'atelier' en ce moment.</p>");
    }

    if ($html)
        $output = sprintf("<div class='maia-spectacles-container'>%s</div>", $html);
    return $output;
}

/**
 * Shortcode pour afficher les articles du blog
 */
add_shortcode('retrieve_blog', 'maia_blog_2');
function maia_blog_2 ($atts) {
    $html   = "";
    $output = "";
    $index  = 1;

    $atts = shortcode_atts( array(
		'cat'   => '',
        'page'  => 0,
	), $atts, 'retrieve_blog' );

    $posts_per_page = get_option('posts_per_page');
    $cat_ID         = $atts['cat'];
    $offset         = $atts['page'] * $posts_per_page;
    $next_page      = ($atts['page']+1) * $posts_per_page;
    $count_posts    = wp_count_posts();

    if ( $count_posts ) {
        $published_posts = $count_posts->publish;
    }

    if ($cat_ID) {
        $published_posts = get_category($cat_ID)->category_count;;
    }

    /**
     * Je récupère les articles
     */
    $articles = get_posts (
        array (
            'post_status'           => 'publish',
            'post_type'             => 'post',
            'posts_per_page'        => $posts_per_page,
            'no_found_rows'         => true,
            'category'              => $cat_ID,
            'offset'                => $offset,
            'order'                 => 'DESC'
        )
    );

    if (count($articles)) {
        foreach ($articles as $article) {
            $post_id       = $article->ID;
            $pe_title      = apply_filters('the_title', $article->post_title);
            $pe_content    = apply_filters('the_excerpt', $article->post_excerpt);
            $pe_url        = get_post_permalink($post_id);

            $html .= sprintf("<article id='actu-%d' class='actu'>", $post_id);
//            if (has_post_thumbnail($post_id))
//                $html .= sprintf("<a href='%s'>%s</a>", $pe_url, get_the_post_thumbnail($post_id, 'large'));
            $html .= sprintf("<a href='%s'>%s</a>", $pe_url, get_the_category_thumbnail(wp_get_post_categories($post_id)[0]));

            $html .= "<div class='actu-content-wrap'>";
            $html .= sprintf("<h2><a href='%s'>%s</a></h2>", $pe_url, $pe_title) ;
            $html .= sprintf("<div class='excerpt'>%s</div>", $pe_content);
            $html .= "</div> <!-- actu-content-wrap -->";
            $html .= "<div class='actu-footer-wrap'>";
            $html .= sprintf("<div class='actu-suite'><a href='%s'>Lire la suite</a></div>", $pe_url);
            $html .= sprintf("<div class='actu-category'><a href='%s'>%s</a></div>", esc_attr( esc_url( get_category_link( wp_get_post_categories($post_id)[0] ) ) ), get_category(wp_get_post_categories($post_id)[0])->name);
            $html .= "</div> <!-- actu-footer-wrap -->";
            $html .= sprintf("</article>");

            $index++;
        }
        if ( $next_page < $published_posts && $atts['page'] == 0) {
            $button = sprintf('<button class="next-blog" data-cat-id="%s" data-per-page="%s" data-max-posts="%s" data-current-page="%s" data-nonce="%s" data-action="maia_next_page" data-ajaxurl="%s">Paroles suivantes</button>', $cat_ID, $posts_per_page, $published_posts, $atts['page'], wp_create_nonce('maia_next_page'), admin_url( 'admin-ajax.php' ));
        }

    } else {
        $html .= sprintf("404");
    }

    if ($html)
        if ($atts['page'] == 0)
            $output = sprintf("<div class='actus-wrap'>%s</div>%s", $html, $button);
        else
            $output = sprintf("%s", $html);
        
    return $output;
}

add_action( 'wp_ajax_maia_next_page', 'maia_next_page' );
add_action( 'wp_ajax_nopriv_maia_next_page', 'maia_next_page' );

function maia_next_page() {
  
	// Vérification de sécurité
  	if( 
		! isset( $_REQUEST['nonce'] ) or 
       	! wp_verify_nonce( $_REQUEST['nonce'], 'maia_next_page' ) 
    ) {
    	wp_send_json_error( "Vous n’avez pas l’autorisation d’effectuer cette action.", 403 );
  	}
    
    // On vérifie que l'identifiant a bien été envoyé
    if( ! isset( $_POST['page'] ) ) {
    	wp_send_json_error( "L'identifiant de l'article est manquant.", 403 );
  	}

  	// Récupération des données du formulaire
  	$page = intval( $_POST['page'] );
  	$catID = intval( $_POST['catid'] );
    
  	// Préparer le HTML des commentaires
  	$html = maia_blog_2 (['page' => $page, 'cat' => $catID]);

  	// Envoyer les données au navigateur
	wp_send_json_success( $html );
}


function maia_blog () {
    $html = "";
    $output = "";
    $index = 1;

    /**
     * Je récupère les articles
     */
    $articles = get_posts (
        array (
            'post_status'           => 'publish',
            'post_type'             => 'post',
            'posts_per_page'        => -1,
            'no_found_rows'         => true,
            'orderby'               => 'order',
            'order'                 => 'ASC'
        )
    );

    if (count($articles)) {
        foreach ($articles as $article) {
            $pe_title      = apply_filters('the_title', $article->post_title);
            $pe_content    = apply_filters('the_excerpt', $article->post_excerpt);
            $pe_url        = get_post_permalink($article->ID);

            if ( get_the_post_thumbnail($article->ID) ) {
                $html .= sprintf("<spectacle id='post-%d' class='%s'>", $article->ID, implode(' ', get_post_class('', $article->ID)));
                $html .= sprintf("<div class='dual rev-%d'>", $index);
                $html .= sprintf("<a href='%s' rel='bookmark'>", esc_url( $pe_url ));
                $html .= sprintf("<div class='dual__inner' id='rev-%d'>", $index);
                $html .= sprintf("<div class='dual__half' style='background-image:url(%s);display:none;'></div>", get_the_post_thumbnail_url($article->ID,  "medium_large" ));
                $html .= sprintf("</div><div class='dual__content'><h2 class='entry-title'>%s</h2>%s</div></a></div></spectacle>", $pe_title, $pe_content);
            } else {
                $html .= sprintf("<div class='entry-content maia-spectacle-entry'><a href='%s' target='_blank'>%s<h4>%s</h4>%s</a></div>",$pe_url, $pe_title, $pe_content);
            }
            $index++;
        }

    } else {
        $html .= sprintf("<p>Pas d'atelier' en ce moment.</p>");
    }

    if ($html)
        $output = sprintf("<div class='maia-spectacles-container'>%s</div>", $html);
    return $output;
}

function maia_posts_navigation () {
    $prev_text = "%title";
    $next_text = "%title";

    $cpts = ['spectacle', 'atelier', 'event'];

    $posts_navigation = get_posts_navigation( array(
        'prev_text'     => $prev_text,
        'next_text'     => $next_text,
    ) );
    ob_start();
// print_r($posts_navigation);
    if ( ! empty( $posts_navigation['prev']->permalink ) ) {
        $prev_link_text = '' !== $prev_text ? $prev_text : $posts_navigation['prev']->title;

        $post_id   = $posts_navigation['prev']->id;
        $post_type = get_post_type($post_id);

        if (is_single() && !in_array($post_type, $cpts)) {
            $posts_navigation['prev']->thumblink = get_the_category_data(wp_get_post_categories($post_id)[0])->sizes->thumbnail->url;
        }

        ?>
            <div class="nav-previous" data-thumb="<?php echo esc_url( $posts_navigation['prev']->thumblink ); ?>">
                <a href="<?php echo esc_url( $posts_navigation['prev']->permalink ); ?>" rel="prev">
                    <span class="nav-prev-thumb" style="background-image:url('<?php echo esc_url( $posts_navigation['prev']->thumblink ); ?>');"></span>
                    <span class="meta-nav">&larr;&nbsp;</span><span><?php echo esc_html( $posts_navigation['prev']->title ); ?></span>
                </a>
            </div>
        <?php
    } else {
        echo '<div class="nav-previous" data-thumb=""></div>';
    }

    if ( ! empty( $posts_navigation['next']->permalink ) ) {
        $next_link_text = '' !== $next_text ? $next_text : $posts_navigation['next']->title;
        
        $post_id   = $posts_navigation['next']->id;
        $post_type = get_post_type($post_id);

        if (is_single() && !in_array($post_type, $cpts)) {
            $posts_navigation['next']->thumblink = get_the_category_data(wp_get_post_categories($post_id)[0])->sizes->thumbnail->url;
        }


        ?>
            <div class="nav-next" data-thumb="<?php echo esc_url( $posts_navigation['next']->thumblink ); ?>">
                <a href="<?php echo esc_url( $posts_navigation['next']->permalink ); ?>" rel="next">
                    <span><?php echo esc_html( $posts_navigation['next']->title ); ?></span><span class="meta-nav">&nbsp;&rarr;</span>
                    <span class="nav-next-thumb" style="background-image:url('<?php echo esc_url( $posts_navigation['next']->thumblink ); ?>');"></span>
                </a>
            </div>
        <?php
    } else {
        echo '<div class="nav-next" data-thumb=""></div>';
    }

    $page_links = ob_get_contents();

    ob_end_clean();

    $output = sprintf(
        '<div id="navigation-post">
            %1$s
        </div>',
        $page_links
    );

    echo $output;

}

/**
* Get prev and next post link data for frontend builder's post navigation module component
*
* @param int    post ID
* @param bool   show posts which uses same link only or not
* @param string excluded terms name
* @param string taxonomy name for in_same_terms
*
* @return string JSON encoded array of post's next and prev link
*/
function get_posts_navigation( $args = array(), $conditional_tags = array(), $current_page = array() ) {
    global $post;

    $defaults = array(
        'in_same_term'   => 'off',
        'taxonomy_name'  => 'category',
        'prev_text'      => '%title',
        'next_text'      => '%title',
    );

    $args = wp_parse_args( $args, $defaults );

    // taxonomy name overwrite if in_same_term option is set to off and no taxonomy name defined
    if ( '' === $args['taxonomy_name'] || 'on' !== $args['in_same_term'] ) {
        $is_singular_project   = isset( $conditional_tags['is_singular_project'] ) ? $conditional_tags['is_singular_project'] === 'true' : is_singular( 'project' );
        $args['taxonomy_name'] = $is_singular_project ? 'project_category' : 'category';
    }

    $in_same_term = ! $args['in_same_term'] || 'off' === $args['in_same_term'] ? false : true;

    if ( ! isset( $post ) && defined( 'DOING_AJAX' ) && DOING_AJAX && ! empty( $_POST['et_post_id'] ) ) {
        $post_id = sanitize_text_field( $_POST['et_post_id'] );
    } else {
        $post_id = $post->ID;
    }

    // Overwrite global $post value in this scope
    if ( isset( $current_page['id'] ) ) {
        $post_id = intval( $current_page['id'] );
    }

    // Set current post as global $post
    $post = get_post( $post_id );

    // Get next post
    $next_post = get_next_post( $in_same_term, '', $args['taxonomy_name'] );

    $next = new stdClass();

    if ( ! empty( $next_post ) ) {

        $next_title = isset($next_post->post_title) ? esc_html( $next_post->post_title ) : esc_html__( 'Next Post' );

        $next_date = mysql2date( get_option( 'date_format' ), $next_post->post_date );
        $next_permalink = isset($next_post->ID) ? esc_url( get_the_permalink( $next_post->ID ) ) : '';

        $next_processed_title = '' === $args['next_text'] ? '%title' : $args['next_text'];

        // process Wordpress' wildcards
        $next_processed_title = str_replace( '%title', $next_title, $next_processed_title );
        $next_processed_title = str_replace( '%date', $next_date, $next_processed_title );
        $next_processed_title = str_replace( '%link', $next_permalink, $next_processed_title );

        $next->title = $next_processed_title;
        $next->id = isset($next_post->ID) ? intval( $next_post->ID ) : '';
        $next->permalink = $next_permalink;
        $next->thumblink = get_the_post_thumbnail_url($next_post->ID, "thumbnail");
    } else {
        $next->title = "";
        $next->id = "";
        $next->permalink = "";
        $next->thumblink = "";
    }

    // Get prev post
    $prev_post = get_previous_post( $in_same_term, '', $args['taxonomy_name'] );

    $prev = new stdClass();

    if ( ! empty( $prev_post ) ) {

        $prev_title = isset($prev_post->post_title) ? esc_html( $prev_post->post_title ) : esc_html__( 'Previous Post' );

        $prev_date = mysql2date( get_option( 'date_format' ), $prev_post->post_date );

        $prev_permalink = isset($prev_post->ID) ? esc_url( get_the_permalink( $prev_post->ID ) ) : '';

        $prev_processed_title = '' === $args['prev_text'] ? '%title' : $args['prev_text'];

        // process Wordpress' wildcards
        $prev_processed_title = str_replace( '%title', $prev_title, $prev_processed_title );
        $prev_processed_title = str_replace( '%date', $prev_date, $prev_processed_title );
        $prev_processed_title = str_replace( '%link', $prev_permalink, $prev_processed_title );

        $prev->title = $prev_processed_title;
        $prev->id = isset($prev_post->ID) ? intval( $prev_post->ID ) : '';
        $prev->permalink = $prev_permalink;
        $prev->thumblink = get_the_post_thumbnail_url($prev_post->ID, "thumbnail");
    }else {
        $prev->title = "";
        $prev->id = "";
        $prev->permalink = "";
        $prev->thumblink = "";
    }

    // Formatting returned value
    $posts_navigation = array(
        'next' => $next,
        'prev' => $prev,
    );

    return $posts_navigation;
}

/*
 * https://wordpress.stackexchange.com/questions/73190/can-the-next-prev-post-links-be-ordered-by-menu-order-or-by-a-meta-key
 * Plugin Name: (#73190) Alter adjacent post link sort order 
 */
function babelweb_adjacent_post_where($sql) {
    if ("projet" == get_post_type()) {
        $the_post = get_post( get_the_ID() );
        $patterns = array();
        $patterns[] = '/post_date/';
        $patterns[] = '/\'[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}\'/';
        $replacements = array();
        $replacements[] = 'menu_order';
        $replacements[] = $the_post->menu_order;
        return preg_replace( $patterns, $replacements, $sql );
    }
    return $sql;
}

add_filter( 'get_next_post_where', 'babelweb_adjacent_post_where' );
add_filter( 'get_previous_post_where', 'babelweb_adjacent_post_where' );

function babelweb_adjacent_post_sort( $sql ) {
    if ("spectacle" == get_post_type() or "atelier" == get_post_type()) {
        $pattern = '/post_date/';
        $replacement = 'menu_order';

        return preg_replace( $pattern, $replacement, $sql );
    }
    return $sql;
}
add_filter( 'get_previous_post_sort', 'babelweb_adjacent_post_sort' );
add_filter( 'get_next_post_sort', 'babelweb_adjacent_post_sort' );


/*
 * Affiche le menu des catégories
 */
add_shortcode('retrieve_cat_blog', 'display_cat_blog');
function display_cat_blog() {
    $html = "<ul class='menu_cat'>";
    ob_start();

	wp_list_categories( array(
		'orderby'    => 'name',
		'show_count' => true,
        'title_li'   => '',
	) );

    $html .= ob_get_contents();
    ob_end_clean();
  
    $html .= "</ul>";

    return $html;
}


?>