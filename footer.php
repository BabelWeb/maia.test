<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package maia
 */

?>
<?php
    if (is_single()) {
        echo maia_posts_navigation();
    }
?>

	</div><!-- #content -->

<?php  
    if (is_page("contact")) {
?>
    <div id="agenda" class="wp-block-group">
        <div class="wp-block-group__inner-container">
            <div class="wp-block-group groupe-centre">
                <div class="wp-block-group__inner-container">
                    <h2 class="has-text-align-center square-title square-jaune">Agenda</h2>
                </div>
            </div>
            <div id="calendrier">
                <?php
                $button = '';
                $shortcode = sprintf('[events_list scope="future"]<div class="event"><div class="ev-rdv"><span class="ev-jour">#d</span><span class="ev-mois">#M</span></div><div class="ev-image" style="background-image:url(#_EVENTIMAGEURL)"></div><div class="ev-name"><a href="#_EVENTURL"><h3>#_EVENTNAME</h3></a><div class="ev-content">#_EVENTNOTES</div></div><div class="ev-date"><span class="ev-times">#_EVENTTIMES</span>{has_tarif}<span class="ev-tarif">#_ATT{prix}</span>{/has_tarif}</div></div>[/events_list]', $button);
                ?>
                <?php echo do_shortcode($shortcode); ?>
            </div>
        </div>
    </div>
<?php
    }
?>

	<footer id="colophon" class="site-footer">
		<div class="site-info">
            Ce site a été conçu et réalisé par <a href="https://www.babel-web.info" target="_blank">Babel-Web</a>
		</div><!-- .site-info -->
        <!--
		<div class="credits-photo">
            Crédits photos : &copy; <a href="http://www.film-documentaire.fr/4DACTION/w_liste_generique/C_76454_F" target="_blank">Pauline Fonsny</a>, Yann Sevrin, Freddy Vandenbrouck, <a href="https://www.instagram.com/fabienne_pennewaert/?hl=fr" target="_blank">Fabienne Pennewaert</a>
		</div>
        --><!-- .credits-photo -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.7.1/gsap.min.js"></script>
<?php 
    if (is_single()) :
?>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/p5.js/0.8.0/p5.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/p5.js/0.8.0/addons/p5.dom.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/p5.js/0.8.0/addons/p5.sound.min.js"></script>
<?php
    endif;
?>

<?php wp_footer(); ?>

</body>
</html>
