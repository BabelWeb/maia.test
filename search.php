<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package maia
 */

get_header(); 
$cpts = ['spectacle', 'atelier', 'event', 'page'];
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main content">
            <header class="page-header">
                <h1 class="page-title">
                    <?php printf( 'Résultats de la recherche pour : <span>%s</span>', get_search_query()); ?>
                </h1>
            </header><!-- .page-header -->

		<?php
            if ( have_posts() ) :
                while ( have_posts() ) : the_post();

                    $post_id = get_the_ID();
                    $post_type = get_post_type();
                    $html = "";

                    if (!in_array($post_type, $cpts)) {
                        $thumblink = get_the_category_data(wp_get_post_categories($post_id)[0])->sizes->medium->url;
                    }
                    else {
                        $thumblink = get_the_post_thumbnail_url($post_id);
                    }
        
                    if (wp_get_post_categories($post_id)) {
                        $category = get_category_link( wp_get_post_categories($post_id)[0] );
                        $cat_name = get_category(wp_get_post_categories($post_id)[0])->name;
                    } else {
                        $cat_name = get_post_type();
                    }

                    $content  = apply_filters( 'the_excerpt', get_the_excerpt("", "", $post_id) );
                    $title    = get_the_title();
                    $post_link = get_post_permalink();

                    $html = sprintf('<article class="search__article single_%s" id="content-%s">
                        <h3 class="heading">%s</h3>
                        <div class="content__text">
                            <div class="thumbnail" style="background-image:url(%s);"><a href="%s"></a></div>
                            <div class="content__excerpt">
                                <h2><a href="%s">%s</a></h2>
                                %s
                            </div>
                        </div></a>
                    </article>', $post_type, $post_id, $cat_name, esc_url( esc_attr( $thumblink )), esc_url( esc_attr( $post_link )), esc_url( esc_attr( $post_link )), $title, $content);
                    echo $html;
                endwhile;
            else :
                get_template_part( 'template-parts/content', 'none' );
            endif; 
        ?>
        
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
