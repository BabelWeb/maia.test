jQuery(document).ready(function($) {
    /* 
    * Scroll Bar on Textarea in Contact Form
    */
    $('.scrollbar-dynamic').scrollbar();

    /* 
    * Affiche le formulaire
    */
    $(".wpcf7-form").addClass("anim-box");

    [].slice.call( document.querySelectorAll( 'input.input__field, textarea.input__field' ) ).forEach( function( inputEl ) {
        if( inputEl.value.trim() !== '' ) {
            inputEl.parentNode.parentNode.classList.add('input--filled');
        }

        // events:
        inputEl.addEventListener( 'focus', onInputFocus );
        inputEl.addEventListener( 'blur', onInputBlur );
    } );
        
    function onInputFocus( ev ) {
        var cName = ev.target.nodeName,
            el = ev.target.parentNode.parentNode;

        if (cName.indexOf("TEXTAREA") != -1) {
            var el = this, i = 4;
            while(i-- && (el = el.parentNode));
        }
        el.classList.add('input--filled');
    }

    function onInputBlur( ev ) {
        if( ev.target.value.trim() === '' ) {
            var cName = ev.target.nodeName,
                el = ev.target.parentNode.parentNode;

            if (cName.indexOf("TEXTAREA") != -1) {
                var el = this, i = 4;
                while(i-- && (el = el.parentNode));
            }
            el.classList.remove('input--filled');
        }
    }
        
});
