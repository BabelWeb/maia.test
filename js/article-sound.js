const s = (p) => {
    let demo1Shader, img, fft, audio, toggleBtn
  
    p.preload = () => {
      audio = p.loadSound('/wp-content/themes/maia/css/sons/mer-1.wav')
      demo1Shader = p.loadShader('/wp-content/themes/maia/css/shaders/base.vert', '/wp-content/themes/maia/css/shaders/d1.frag')
      img = p.loadImage(sound_image.image_url)
    }
  
    p.setup = () => {
        document.body.classList.add('start-anim');
        audio.loop();
  
        p.pixelDensity(1)
        p.createCanvas(p.windowWidth/2, p.windowHeight/2, p.WEBGL)
  
        toggleBtn = document.querySelector('#toggle-btn')
        toggleBtn.addEventListener('click', () => {
            toggleBtn.classList.toggle('toggle--on')
            this.toggleAudio()
        })
  
        fft = new p5.FFT()
        p.shader(demo1Shader)
  
        demo1Shader.setUniform('u_resolution', [p.windowWidth/2 - 20, p.windowHeight/2 -20])
        demo1Shader.setUniform('u_texture', img)
        demo1Shader.setUniform('u_tResolution', [img.width, img.height])
    }
  
    p.draw = () => {
      fft.analyze()
  
      const bass = fft.getEnergy("bass")
      const treble = fft.getEnergy("treble")
      const mid = fft.getEnergy("mid")
  
      const mapBass = p.map(bass, 0, 255, 10, 15.0)
      const mapTremble = p.map(treble, 0, 255, 0, 0.0)
      const mapMid = p.map(mid, 0, 255, 0.0, 0.1)
  
      demo1Shader.setUniform('u_time', p.frameCount / 20)
      demo1Shader.setUniform('u_bass', mapBass)
      demo1Shader.setUniform('u_tremble', mapTremble)
      demo1Shader.setUniform('u_mid', mapMid)
  
      p.rect(0,0, p.width, p.height)
    }
  
    p.windowResized = () => {
        p.resizeCanvas(p.windowWidth, p.windowHeight)
        demo1Shader.setUniform('u_resolution', [p.windowWidth/2 - 20, p.windowHeight/2 - 20])
    }
  
    toggleAudio = () => {
        if (audio.isPlaying()) {
            audio.pause()
        } else {
            audio.loop()
        }
    }
};

new p5(s);