import Screen from "./screen";
// import { gsap } from 'gsap';


export default class Navigation {
    constructor(el) {
        this.DOM = {
            links : [...el.querySelectorAll('.menu__item > a')]
        }
        this.screens = [];
        [...document.querySelectorAll('.screen__item')].forEach(el => this.screens.push(new Screen(el)));
        this.current = 0;
        this.isPassed = false;
        this.sounds = [];
        this.initEvents();
    }
    initEvents() {
        this.onMouseEnter = (ev) => {
            const position = this.DOM.links.indexOf(ev.target);

            const son_idx = position + 1;
            document.getElementById("audio-"+son_idx).volume = 1;
            document.getElementById("audio-"+son_idx).currentTime = 0;
            // to be clicked to success play()
            document.getElementById("page").click();
            var promise = document.getElementById("audio-"+son_idx).play();

            if (promise !== undefined) {
                promise.then(_ => {
                    console.log("Autoplay already started!");
                }).catch(error => {
                    // Autoplay was prevented.
                    document.querySelector('#toggle-btn').style.display = "block"; 
                    if (!this.isPassed) {
                        alert("Veuillez cliquer sur le bouton son en haut à droite pour entendre l'ambiance ! Merci ;-)");
                        this.isPassed = true;
                        console.log("Autoplay started!");
                    }
                    
                });
            }
//            document.getElementById("audio-"+son_idx).play();

            if ( position === this.current ) {
                return false;
            }
            const currentScreen = this.screens[this.current];
            const nextScreen = this.screens[position];

            this.current = position;
            
            gsap.killTweensOf([currentScreen.DOM.el, nextScreen.DOM.el]);
            this.hideScreen(currentScreen)
            this.showScreen(nextScreen);
        };

        this.onMouseOut = (ev) => {

        };
        this.DOM.links.forEach((link) => {
            link.addEventListener('mouseenter', this.onMouseEnter);
            link.addEventListener('mouseout', this.onMouseOut);
        });
    }
/*
    getSoundAndFadeAudio (sound) {
//        var sound = document.getElementById(audiosnippetId);
console.log(sound.currentTime);
console.log(sound.volume.toFixed(1));

    
        // Set the point in playback that fadeout begins. This is for a 2 second fade out.
        var fadePoint = sound.duration - 2; 
        var fadeAudio = setInterval(function () {
    
            // Only fade if past the fade out point or not at zero already
            let vol = sound.volume.toFixed(1);
            if ((sound.currentTime <= fadePoint) && (vol != 0.0)) {
                sound.volume -= 0.05;
            }
            // When volume at zero stop all the intervalling
            if (vol <= 0.0) {
                sound.currentTime = 0;
                sound.pause();
                clearInterval(fadeAudio);
            }
        }, 50);
    
    }

/*    
    audioVolumeOut(q){
        if(q.volume){
           var InT = 0.4;
           var setVolume = 0;  // Target volume level for old song 
           var speed = 0.005;  // Rate of volume decrease
           q.volume = InT;
           var fAudio = setInterval(function(){
               InT -= speed;
               q.volume = InT.toFixed(1);
               if(InT.toFixed(1) <= setVolume){
                  clearInterval(fAudio);
                  //alert('clearInterval fAudio'+ InT.toFixed(1));
               };
           },50);
        };
    };    
*/
    showScreen(screen) { 
        gsap.timeline()
            .set(screen.DOM.el, {opacity: 1, zIndex: 1})
            .to(screen.DOM.full, {
                duration: 1.8,
                ease: "Power2.easeOut",
                startAt: {scale: 1.07},
                scale: 1
            });
    }
    hideScreen(screen) {
        gsap.timeline()
            .set(screen.DOM.el, {zIndex: 10})
            .to(screen.DOM.el, {
                duration: 0.4,
                ease: "Power2.easeOut",
                opacity: 0,
                onComplete: () => gsap.set(screen.DOM.el, {zIndex: 1})
            });
    }
}
