(function ($) {
    $(document).ready(function () {
  
        $('button.next-blog').click(function (e) {
//            e.preventDefault();

            const ajaxurl = $(this).data('ajaxurl');
            const button  = $(this);

            const data = {
                action:  $(this).data('action'), 
                nonce:   $(this).data('nonce'),
                current: $(this).data('current-page'),
                perPage: $(this).data('per-page'),
                maxPost: $(this).data('max-posts'),
                catid:   $(this).data('cat-id'),
            };

            var isEnd = false;
            if (data.perPage * (button.data('current-page') + 2) >= data.maxPost)
                isEnd = true;

            $.ajax({
                url : ajaxurl,
                method : 'POST',
                data : {
                    action : data.action,
                    page   : data.current + 1,
                    nonce  : data.nonce,
                    catid  : data.catid
                },
                success : function( data ) {
                    if ( data.success ) {
                        $('.actus-wrap').append( data.data );
                        var pageNext = button.data('current-page') + 1;
                        button.data('current-page', pageNext);

                        var nice = $("body").getNiceScroll();
                        $("body").getNiceScroll().resize();
                        if (isEnd) {
                            button.remove();
                        }
                    } else {
                        console.log( data.data );
                    }
                },
                error : function( data ) {
                    console.log( 'Erreur…' );
                }
            });
        });
  
    });
  })(jQuery);