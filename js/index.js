//import Utils from "./utils";
import Navigation from "./navigation";
import AnimatedLinksObjs from "./animatedLink";
import { Item, ItemPage }  from './item';

// Preload images
const preloadImages = () => {
    return new Promise((resolve, reject) => {
        imagesLoaded(document.querySelectorAll('.screen'), {background: true}, resolve);
    });
};


// Preload fonts and images
Promise.all([preloadImages()]).then(() => {
    if (document.querySelector('.menu'))
        new Navigation(document.querySelector('.menu'));
    // Remove loader (loading class)
    document.body.classList.remove('loading');

    let itemsArr = [];
    
    if ( window.location.pathname == '/' ) {
        [...document.querySelectorAll('.items .menu__item')].forEach(item => itemsArr.push(new Item(item, itemsArr)));
    } else {
        [...document.querySelectorAll('.content__article')].forEach(item => itemsArr.push(new ItemPage(item, itemsArr)));
    }


});


[...document.querySelectorAll('a.menu__link')].forEach((el) => {
    const elPosition = [...el.parentNode.children].indexOf(el);
//    var elPosition = el.classList[2].slice(-1);
    const fxObj = AnimatedLinksObjs[elPosition];
    fxObj && new fxObj(el);
});

var toggleBtn = document.querySelector('#toggle-btn');
if (toggleBtn)
    toggleBtn.addEventListener('click', () => {
        toggleBtn.classList.toggle('toggle--on');
        toggleBtn.remove();
    });