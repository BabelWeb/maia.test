
jQuery(document).ready(function($) {
    /* 
    * Nice Scroll Bar
    */
    $("body").niceScroll({
        cursorcolor:"#e88e41",
        cursoropacitymax:0.7,
        cursorwidth:"10px",
        background:"#ccc",
        autohidemode:false,
        hwacceleration: false,
        horizrailenabled: false,
        railoffset: true,
    });
});