<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package maia
 */

get_header("front"); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main content">

		<?php
		if ( have_posts() ) :
			while ( have_posts() ) : the_post();
                    $object_id = get_post_meta( $m->ID, '_menu_item_object_id', true );
                    $subtitle  = get_field('sous_titre', get_the_ID());
                    $content   = apply_filters( 'the_content', get_the_content("", "", get_the_ID()) );
                    $title     = get_the_title();
                    $html .= sprintf('<article class="content__article content__article--open" id="content-%s"><h2 class="heading"><span data-splitting>%s</span><span data-splitting>%s</span></h2><div class="content__text">%s</div></article>', $object_id, $title, $subtitle, $content);
                    echo $html;
			endwhile;
		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>
        
            <button class="content__back unbutton">
                <svg width="108" height="23" viewBox="0 0 108 23">
                    <path stroke="#000" fill="none" d="M107.5 11.5H1.5M1.5 11.5c8.975-.536 15.087-1.364 18.336-2.484C23.086 7.896 26.64 5.39 30.5 1.5M1.5 11.5c8.975.536 15.087 1.364 18.336 2.484 3.25 1.12 6.804 3.626 10.664 7.516"/>
                </svg>
            </button>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();
